:-dynamic process_request/4.
:-dynamic process_notify/2.
:-dynamic carico_attuale/2.
:-dynamic attivo/3.
:-dynamic retract_process_request/0.


carico_attuale(temp, 0).
carico_massimo(temp, 150).

consumo(condizionatore, 2).
consumo(deumidificatore, 2).
consumo(lampada, 5).

requestE(Agent, Act, Status, Delta):> 
				assert(process_request(Agent, Act, Status, Delta)).

notifyE(Agent, Act):> 
				write('Nuova notifica in arrivo:'), write(Agent), write(' '),
				write(Act), nl,
				assert(process_notify(Agent, Act)).

process_requestI(Agent, Act, Status, Delta):> 
				assert(retract_process_request),
				Status = 1, 
				carico_attuale(Agent, Curr), 
				consumo(Act, Fattore), 
				carico_massimo(Agent, M),
				New_Curr is round(Curr + (Delta * Fattore)),
				New_Curr =< M,
				write('Controller: '), write(Agent), write(' ha richiesto '), write(Act), write(' '), write(Delta), write(' '),
				write('Carico Corrente: '), write(Curr), write('  Nuovo Corrente: '), write(New_Curr), nl,  
				retract(carico_attuale(Agent, Curr)),
				assert(carico_attuale(Agent, New_Curr)),
				assert(attivo(Agent, Act, Delta)),
				messageA(Agent,send_message(permitted(Act), controller)),
				write('Richiesta Accettata'), nl.

process_requestI(Agent, Act, Status, Delta):> 
				assert(retract_process_request),
				Status = 1, 
				carico_attuale(Agent, Curr),
				consumo(Act, Fattore), 
				carico_massimo(Agent, M),
				New_Curr is round(Curr + (Delta * Fattore)),
				New_Curr > M,
				write('Controller: '), write(Agent), write(' ha richiesto '), write(Act), write(' '), write(Delta), write(' '),
				write('Carico Corrente: '), write(Curr), write('  Nuovo Corrente: '), write(New_Curr), nl,
				messageA(Agent,send_message(denied(Act), controller)),
				write('Richiesta Rifiutata'), nl.

process_notifyI(Agent, Act):> 
				carico_attuale(Agent, Curr),
				consumo(Act, Fattore), 
				attivo(Agent, Act, Delta),
				New_Curr is round(Curr - (Delta * Fattore)),
				retract(carico_attuale(Agent, Curr)),
				assert(carico_attuale(Agent, New_Curr)),
				retract(attivo(Agent, Act, Delta)),
				write('Nuovo Carico Corrente: '), write(New_Curr), nl,
				write('Notifica Spegnimento'), nl.

retract_process_requestI:> 
				retractall(process_request(_,_,_,_)).

