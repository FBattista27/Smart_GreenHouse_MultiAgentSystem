
:-compile('../mas/redis_client.pl').

:-dynamic too_high_temp/4.

:-dynamic too_low_umid/4.

:-dynamic too_high_umid/4.

:-dynamic outer_umid_good/4.

:-dynamic outer_umid_not_good/4.

:-dynamic sportello/1.

:-dynamic deumidificatore/1.

:-dynamic condizionatore/1.

:-dynamic lampada/1.

:-dynamic rete/1.

:-dynamic irrigatore/1.

:-dynamic temperatura_interna/4.

:-dynamic temperatura_esterna/4.

:-dynamic umidita_aria_esterna/4.

:-dynamic umidita_aria_interna/4.

:-dynamic umidita_terreno/4.

:-dynamic intensita_luminosa/4.

:-dynamic vento/4.

:-dynamic pluviometro/4.

:-dynamic piove/0.

:-dynamic non_pove/0.

:-dynamic si_luce/4.

:-dynamic no_luce_int/4.

:-dynamic no_luce_est/4.

:-dynamic too_low_luce/4.

:-dynamic too_low_umid_terreno/4.

:-dynamic too_high_luce/4.

:-dynamic good_luce/4.

:-dynamic retract_intensita_luminosa/0.

:-dynamic retract_umidita_interna/0.

:-dynamic retract_temperatura_interna/0.

:-dynamic retract_temperatura_esterna/0.

:-dynamic retract_pluviometro/0.

:-dynamic retract_outer_temp_good/0.

:-dynamic retract_umidita_terreno/0.

:-dynamic outer_temp_good/4.

:-dynamic outer_temp_not_good/4.

:-dynamic permitted_activation/1.

:-dynamic requested/1.

:-dynamic valore_deumidificatore/1.

:-dynamic retract_permitted_activation/0.

eve(temperatura_interna(var_V,var_Area,var_ID,var_Timestamp)):-assert(temperatura_interna(var_V,var_Area,var_ID,var_Timestamp)).

eve(temperatura_esterna(var_V,var_Area,var_ID,var_Timestamp)):-assert(temperatura_esterna(var_V,var_Area,var_ID,var_Timestamp)).

eve(umidita_terreno(var_V,var_Area,var_ID,var_Timestamp)):-assert(umidita_terreno(var_V,var_Area,var_ID,var_Timestamp)).

eve(umidita_aria_interna(var_V,var_Area,var_ID,var_Timestamp)):-assert(umidita_aria_interna(var_V,var_Area,var_ID,var_Timestamp)).

eve(umidita_aria_esterna(var_V,var_Area,var_ID,var_Timestamp)):-assert(umidita_aria_esterna(var_V,var_Area,var_ID,var_Timestamp)).

eve(pluviometro(var_V,var_Area,var_ID,var_Timestamp)):-assert(pluviometro(var_V,var_Area,var_ID,var_Timestamp)).

eve(intensita_luminosa(var_V,var_Area,var_ID,var_Timestamp)):-assert(intensita_luminosa(var_V,var_Area,var_ID,var_Timestamp)).

eve(vento(var_V,var_Area,var_ID,var_Timestamp)):-assert(vento(var_V,var_Area,var_ID,var_Timestamp)).

eve(permitted(var_X)):-write('Richiesta accettata'),nl,assert(permitted_activation(var_X)).

eve(denied(var_X)):-retract(requested(var_X)),write('Richiesta '),write(var_X),write('rifiutata'),nl.

too_high_temp(var_Value,var_Area,var_ID,var_Timestamp):-temperatura_interna(var_Value,var_Area,var_ID,var_Timestamp),max_temp(var_Th),var_Value>var_Th.

evi(too_high_temp(var_Value,var_Area,var_ID,var_Timestamp)):-assert(retract_temperatura_interna),mas_send(alarm(var_Area,var_ID,temperatura_interna,var_Value,var_Timestamp)),condizionatore(0),\+requested(condizionatore),var_Delta is round(var_Value-20),a(message(controller,send_message(request(temp,condizionatore,1,var_Delta),temp))),assert(requested(condizionatore)).

evi(permitted_activation(condizionatore)):-retract(requested(condizionatore)),mas_send(activate(condizionatore,1,2,[1,20])),retract(condizionatore(0)),assert(condizionatore(1)),retract(permitted_activation(condizionatore)).

evi(retract_temperatura_interna):-retractall(temperatura_interna(var__,var__,var__,var__)).

outer_temp_good(var_Value,var_Area,var_ID,var_Timestamp):-temperatura_esterna(var_Value,var_Area,var_ID,var_Timestamp),var_Value>=12,var_Value=<20.

evi(outer_temp_good(var_Value,var_Area,var_ID,var_Timestamp)):-assert(retract_temperatura_esterna),assert(retract_outer_temp_good),condizionatore(1),a(message(controller,send_message(notify(temp,condizionatore),temp))),mas_send(activate(condizionatore,1,2,[0,0])),retract(condizionatore(1)),assert(condizionatore(0)).

evi(retract_outer_temp_good):-retractall(outer_temp_good(var__,var__,var__,var__)).

outer_temp_not_good(var_Value,var_Area,var_ID,var_Timestamp):-temperatura_esterna(var_Value,var_Area,var_ID,var_Timestamp),var_Value<12.

outer_temp_not_good(var_Value,var_Area,var_ID,var_Timestamp):-temperatura_esterna(var_Value,var_Area,var_ID,var_Timestamp),var_Value>20.

evi(outer_temp_not_good(var_Value,var_Area,var_ID,var_Timestamp)):-assert(retract_temperatura_esterna).

evi(retract_temperatura_esterna):-retractall(temperatura_esterna(var__,var__,var__,var__)).

too_low_umid(var_Value,var_Area,var_ID,var_Timestamp):-umidita_aria_interna(var_Value,var_Area,var_ID,var_Timestamp),min_umidita_aria(var_Min),var_Value<var_Min.

evi(too_low_umid(var_Value,var_Area,var_ID,var_Timestamp)):-assert(retract_umidita_interna),mas_send(alarm(var_Area,var_ID,umidita_aria_interna,var_Value,var_Timestamp)),sportello(1),mas_send(activate(sportello,1,1,[0])),retract(sportello(1)),assert(sportello(0)).

evi(too_low_umid(var_Value,var_Area,var_ID,var_Timestamp)):-assert(retract_umidita_interna),deumidificatore(1),mas_send(activate(deumidificatore,1,3,[0,0])),a(message(controller,send_message(notify(temp,deumidificatore),temp))),retract(deumidificatore(1)),assert(deumidificatore(0)).

too_high_umid(var_Value,var_Area,var_ID,var_Timestamp):-umidita_aria_interna(var_Value,var_Area,var_ID,var_Timestamp),max_umidita_aria(var_Max),var_Value>var_Max.

evi(too_high_umid(var_Value,var_Area,var_ID,var_Timestamp)):-assert(retract_umidita_interna),mas_send(alarm(var_Area,var_ID,umidita_aria_interna,var_Value,var_Timestamp)),sportello(0),mas_send(activate(sportello,1,1,[1])),retract(sportello(0)),assert(sportello(1)),deumidificatore(1),mas_send(activate(deumidificatore,1,3,[0,0])),a(message(controller,send_message(notify(temp,deumidificatore),temp))),retract(deumidificatore(1)),assert(deumidificatore(0)).

evi(too_high_umid(var_Value,var_Area,var_ID,var_Timestamp)):-assert(retract_umidita_interna),deumidificatore(0),var_Y is round(var_Value-55),assert(valore_deumidificatore(var_Y)),\+requested(deumidificatore),a(message(controller,send_message(request(temp,deumidificatore,1,var_Y),temp))),assert(requested(deumidificatore)).

evi(permitted_activation(deumidificatore)):-retract(requested(deumidificatore)),valore_deumidificatore(var_Y),mas_send(activate(deumidificatore,1,3,[1,var_Y])),retract(valore_deumidificatore(var_Y)),retract(deumidificatore(0)),assert(deumidificatore(1)),assert(retract_permitted_activation),sportello(var_X),var_X>0,mas_send(activate(sportello,1,1,[0])),retract(sportello(1)),assert(sportello(0)).

evi(retract_permitted_activation):-retractall(permitted_activation(var__)).

evi(retract_umidita_interna):-retractall(umidita_aria_interna(var__,var__,var__,var__)).

outer_umid_good(var_Value,var_Area,var_ID,var_Timestamp):-umidita_aria_esterna(var_Value,var_Area,var_ID,var_Timestamp),var_Value>=40,var_Value=<65.

evi(outer_umid_good(var_Value,var_Area,var_ID,var_Timestamp)):-retractall(umidita_aria_esterna(var__,var__,var__,var__)),retractall(outer_umid_not_good(var__,var__,var__,var__)),deumidificatore(1),mas_send(activate(deumidificatore,1,3,[0,0])),a(message(controller,send_message(notify(temp,deumidificatore),temp))),retract(deumidificatore(1)),assert(deumidificatore(0)),sportello(0),mas_send(activate(sportello,1,1,[1])),retract(sportello(0)),assert(sportello(1)).

outer_umid_not_good(var_Value,var_Area,var_ID,var_Timestamp):-umidita_aria_esterna(var_Value,var_Area,var_ID,var_Timestamp),var_Value<40.

outer_umid_not_good(var_Value,var_Area,var_ID,var_Timestamp):-umidita_aria_esterna(var_Value,var_Area,var_ID,var_Timestamp),var_Value>65.

evi(outer_umid_not_good(var_Value,var_Area,var_ID,var_Timestamp)):-retractall(umidita_aria_esterna(var__,var__,var__,var__)),retract(outer_umid_good(var__,var__,var__,var__)).

piove(var_Value,var_Area,var_ID,var_Timestamp):-pluviometro(var_Value,var_Area,var_ID,var_Timestamp),var_Value=1.

non_piove(var_Value,var_Area,var_ID,var_Timestamp):-pluviometro(var_Value,var_Area,var_ID,var_Timestamp),var_Value=0.

evi(piove(var_Value,var_Area,var_ID,var_Timestamp)):-assert(retract_pluviometro),mas_send(alarm(var_Area,var_ID,pluviometro,var_Value,var_Timestamp)),retractall(non_piove),sportello(1),mas_send(activate(sportello,1,1,[0])),retract(sportello(1)),assert(sportello(0)).

evi(non_piove(var_Value,var_Area,var_ID,var_Timestamp)):-retractall(piove),assert(retract_pluviometro).

evi(retract_pluviometro):-retract(pluviometro(var__,var__,var__,var__)).

too_high_luce(var_Value,var_Area,var_ID,var_Timestamp):-intensita_luminosa(var_Value,var_Area,var_ID,var_Timestamp),max_luce(var_Th),var_Value>var_Th.

evi(too_high_luce(var_Value,var_Area,var_ID,var_Timestamp)):-assert(retract_intensita_luminosa),rete(0),mas_send(alarm(var_Area,var_ID,intensita_luminosa,var_Value,var_Timestamp)),mas_send(activate(rete_ombreggiante,1,5,[1])),retract(rete(0)),assert(rete(1)).

no_luce_est(var_Value,var_Area,var_ID,var_Timestamp):-intensita_luminosa(var_Value,var_Area,var_ID,var_Timestamp),var_Value=0.

no_luce_int(var_Value,var_Area,var_ID,var_Timestamp):-intensita_luminosa(var_Value,var_Area,var_ID,var_Timestamp),var_Value=<2,var_Value\=0,rete(1).

evi(no_luce_est(var_Value,var_Area,var_ID,var_Timestamp)):-assert(retract_intensita_luminosa),lampada(0),mas_send(alarm(var_Area,var_ID,intensita_luminosa,var_Value,var_Timestamp)),\+requested(lampada),a(message(controller,send_message(request(temp,lampada,1,1),temp))),assert(requested(lampada)).

evi(permitted_activation(lampada)):-retract(requested(lampada)),mas_send(activate(lampada,1,6,[1])),retract(lampada(0)),assert(lampada(1)),assert(retract_permitted_activation).

evi(no_luce_int(var_Value,var_Area,var_ID,var_Timestamp)):-assert(retract_intensita_luminosa),mas_send(alarm(var_Area,var_ID,intensita_luminosa,var_Value,var_Timestamp)),rete(1),mas_send(activate(rete_ombreggiante,1,5,[0])),retract(rete(1)),assert(rete(0)).

si_luce(var_Value,var_Area,var_ID,var_Timestamp):-intensita_luminosa(var_Value,var_Area,var_ID,var_Timestamp),var_Value\=0.

evi(si_luce(var_Value,var_Area,var_ID,var_Timestamp)):-assert(retract_intensita_luminosa),lampada(1),a(message(controller,send_message(notify(temp,lampada),temp))),mas_send(activate(lampada,1,6,[0])),retract(lampada(1)),assert(lampada(0)).

good_luce(var_Value,var_Area,var_ID,var_Timestamp):-intensita_luminosa(var_Value,var_Area,var_ID,var_Timestamp),max_luce(var_Max),var_Value=<var_Max,var_Value>0.

evi(good_luce(var_Value,var_Area,var_ID,var_Timestamp)):-assert(retract_intensita_luminosa),rete(1),mas_send(activate(rete_ombreggiante,1,5,[0])),retract(rete(1)),assert(rete(0)).

evi(retract_intensita_luminosa):-retractall(intensita_luminosa(var__,var__,var__,var__)).

too_low_umid_terreno(var_Value,var_Area,var_ID,var_Timestamp):-umidita_terreno(var_Value,var_Area,var_ID,var_Timestamp),min_umidita_terreno(var_Min),var_Value<var_Min.

evi(too_low_umid_terreno(var_Value,var_Area,var_ID,var_Timestmap)):-assert(retract_umidita_terreno),mas_send(alarm(var_Area,var_ID,umidita_terreno,var_Value,var_Timestamp)),irrigatore(0),mas_send(activate(irrigatore,1,4,[1])),retract(irrigatore(0)),assert(irrigatore(1)).

good_umid_terreno(var_Value,var_Area,var_ID,var_Timestamp):-umidita_terreno(var_Value,var_Area,var_ID,var_Timestamp),min_umidita_terreno(var_Min),max_umidita_terreno(var_Max),var_Value>=var_Min,var_Value=<var_Max.

evi(good_umid_terreno(var_Value,var_Area,var_ID,var_Timestamp)):-assert(retract_umidita_terreno),irrigatore(1),mas_send(activate(irrigatore,1,4,[0])),retract(irrigatore(1)),assert(irrigatore(0)).

too_high_terreno(var_Value,var_Area,var_ID,var_Timestamp):-umidita_terreno(var_Value,var_Area,var_ID,var_Timestamp),max_umidita_terreno(var_Max),var_Value>var_Max.

evi(too_high_terreno(var_Value,var_Area,var_ID,var_Timestamp)):-assert(retract_umidita_terreno),sportello(0),mas_send(activate(sportello,1,1,[1])),retract(sportello(0)),assert(sportello(1)).

evi(too_high_terreno(var_Value,var_Area,var_ID,var_Timestamp)):-irrigatore(1),mas_send(activate(irrigatore,1,4,[0])),retract(irrigatore(1)),assert(irrigatore(0)).

evi(retract_umidita_terreno):-retractall(umidita_terreno(var__,var__,var__,var__)).

max_luce(8).

max_temp(30).

max_umidita_aria(65).

min_umidita_aria(40).

min_umidita_terreno(30).

max_umidita_terreno(70).

sportello(0).

deumidificatore(0).

condizionatore(0).

rete(0).

lampada(0).

irrigatore(0).

:-dynamic receive/1.

:-dynamic send/2.

:-dynamic isa/3.

receive(send_message(var_X,var_Ag)):-told(var_Ag,send_message(var_X)),call_send_message(var_X,var_Ag).

receive(propose(var_A,var_C,var_Ag)):-told(var_Ag,propose(var_A,var_C)),call_propose(var_A,var_C,var_Ag).

receive(cfp(var_A,var_C,var_Ag)):-told(var_Ag,cfp(var_A,var_C)),call_cfp(var_A,var_C,var_Ag).

receive(accept_proposal(var_A,var_Mp,var_Ag)):-told(var_Ag,accept_proposal(var_A,var_Mp),var_T),call_accept_proposal(var_A,var_Mp,var_Ag,var_T).

receive(reject_proposal(var_A,var_Mp,var_Ag)):-told(var_Ag,reject_proposal(var_A,var_Mp),var_T),call_reject_proposal(var_A,var_Mp,var_Ag,var_T).

receive(failure(var_A,var_M,var_Ag)):-told(var_Ag,failure(var_A,var_M),var_T),call_failure(var_A,var_M,var_Ag,var_T).

receive(cancel(var_A,var_Ag)):-told(var_Ag,cancel(var_A)),call_cancel(var_A,var_Ag).

receive(execute_proc(var_X,var_Ag)):-told(var_Ag,execute_proc(var_X)),call_execute_proc(var_X,var_Ag).

receive(query_ref(var_X,var_N,var_Ag)):-told(var_Ag,query_ref(var_X,var_N)),call_query_ref(var_X,var_N,var_Ag).

receive(inform(var_X,var_M,var_Ag)):-told(var_Ag,inform(var_X,var_M),var_T),call_inform(var_X,var_Ag,var_M,var_T).

receive(inform(var_X,var_Ag)):-told(var_Ag,inform(var_X),var_T),call_inform(var_X,var_Ag,var_T).

receive(refuse(var_X,var_Ag)):-told(var_Ag,refuse(var_X),var_T),call_refuse(var_X,var_Ag,var_T).

receive(agree(var_X,var_Ag)):-told(var_Ag,agree(var_X)),call_agree(var_X,var_Ag).

receive(confirm(var_X,var_Ag)):-told(var_Ag,confirm(var_X),var_T),call_confirm(var_X,var_Ag,var_T).

receive(disconfirm(var_X,var_Ag)):-told(var_Ag,disconfirm(var_X)),call_disconfirm(var_X,var_Ag).

receive(reply(var_X,var_Ag)):-told(var_Ag,reply(var_X)).

send(var_To,query_ref(var_X,var_N,var_Ag)):-tell(var_To,var_Ag,query_ref(var_X,var_N)),send_m(var_To,query_ref(var_X,var_N,var_Ag)).

send(var_To,send_message(var_X,var_Ag)):-tell(var_To,var_Ag,send_message(var_X)),send_m(var_To,send_message(var_X,var_Ag)).

send(var_To,reject_proposal(var_X,var_L,var_Ag)):-tell(var_To,var_Ag,reject_proposal(var_X,var_L)),send_m(var_To,reject_proposal(var_X,var_L,var_Ag)).

send(var_To,accept_proposal(var_X,var_L,var_Ag)):-tell(var_To,var_Ag,accept_proposal(var_X,var_L)),send_m(var_To,accept_proposal(var_X,var_L,var_Ag)).

send(var_To,confirm(var_X,var_Ag)):-tell(var_To,var_Ag,confirm(var_X)),send_m(var_To,confirm(var_X,var_Ag)).

send(var_To,propose(var_X,var_C,var_Ag)):-tell(var_To,var_Ag,propose(var_X,var_C)),send_m(var_To,propose(var_X,var_C,var_Ag)).

send(var_To,disconfirm(var_X,var_Ag)):-tell(var_To,var_Ag,disconfirm(var_X)),send_m(var_To,disconfirm(var_X,var_Ag)).

send(var_To,inform(var_X,var_M,var_Ag)):-tell(var_To,var_Ag,inform(var_X,var_M)),send_m(var_To,inform(var_X,var_M,var_Ag)).

send(var_To,inform(var_X,var_Ag)):-tell(var_To,var_Ag,inform(var_X)),send_m(var_To,inform(var_X,var_Ag)).

send(var_To,refuse(var_X,var_Ag)):-tell(var_To,var_Ag,refuse(var_X)),send_m(var_To,refuse(var_X,var_Ag)).

send(var_To,failure(var_X,var_M,var_Ag)):-tell(var_To,var_Ag,failure(var_X,var_M)),send_m(var_To,failure(var_X,var_M,var_Ag)).

send(var_To,execute_proc(var_X,var_Ag)):-tell(var_To,var_Ag,execute_proc(var_X)),send_m(var_To,execute_proc(var_X,var_Ag)).

send(var_To,agree(var_X,var_Ag)):-tell(var_To,var_Ag,agree(var_X)),send_m(var_To,agree(var_X,var_Ag)).

call_send_message(var_X,var_Ag):-send_message(var_X,var_Ag).

call_execute_proc(var_X,var_Ag):-execute_proc(var_X,var_Ag).

call_query_ref(var_X,var_N,var_Ag):-clause(agent(var_A),var__),not(var(var_X)),meta_ref(var_X,var_N,var_L,var_Ag),a(message(var_Ag,inform(query_ref(var_X,var_N),values(var_L),var_A))).

call_query_ref(var_X,var__,var_Ag):-clause(agent(var_A),var__),var(var_X),a(message(var_Ag,refuse(query_ref(variable),motivation(refused_variables),var_A))).

call_query_ref(var_X,var_N,var_Ag):-clause(agent(var_A),var__),not(var(var_X)),not(meta_ref(var_X,var_N,var__,var__)),a(message(var_Ag,inform(query_ref(var_X,var_N),motivation(no_values),var_A))).

call_agree(var_X,var_Ag):-clause(agent(var_A),var__),ground(var_X),meta_agree(var_X,var_Ag),a(message(var_Ag,inform(agree(var_X),values(yes),var_A))).

call_confirm(var_X,var_Ag,var_T):-ground(var_X),statistics(walltime,[var_Tp,var__]),asse_cosa(past_event(var_X,var_T)),retractall(past(var_X,var_Tp,var_Ag)),assert(past(var_X,var_Tp,var_Ag)).

call_disconfirm(var_X,var_Ag):-ground(var_X),retractall(past(var_X,var__,var_Ag)),retractall(past_event(var_X,var__)).

call_agree(var_X,var_Ag):-clause(agent(var_A),var__),ground(var_X),not(meta_agree(var_X,var__)),a(message(var_Ag,inform(agree(var_X),values(no),var_A))).

call_agree(var_X,var_Ag):-clause(agent(var_A),var__),not(ground(var_X)),a(message(var_Ag,refuse(agree(variable),motivation(refused_variables),var_A))).

call_inform(var_X,var_Ag,var_M,var_T):-asse_cosa(past_event(inform(var_X,var_M,var_Ag),var_T)),statistics(walltime,[var_Tp,var__]),retractall(past(inform(var_X,var_M,var_Ag),var__,var_Ag)),assert(past(inform(var_X,var_M,var_Ag),var_Tp,var_Ag)).

call_inform(var_X,var_Ag,var_T):-asse_cosa(past_event(inform(var_X,var_Ag),var_T)),statistics(walltime,[var_Tp,var__]),retractall(past(inform(var_X,var_Ag),var__,var_Ag)),assert(past(inform(var_X,var_Ag),var_Tp,var_Ag)).

call_refuse(var_X,var_Ag,var_T):-clause(agent(var_A),var__),asse_cosa(past_event(var_X,var_T)),statistics(walltime,[var_Tp,var__]),retractall(past(var_X,var__,var_Ag)),assert(past(var_X,var_Tp,var_Ag)),a(message(var_Ag,reply(received(var_X),var_A))).

call_cfp(var_A,var_C,var_Ag):-clause(agent(var_AgI),var__),clause(ext_agent(var_Ag,_653429,var_Ontology,_653433),_653423),asserisci_ontologia(var_Ag,var_Ontology,var_A),once(call_meta_execute_cfp(var_A,var_C,var_Ag,_653467)),a(message(var_Ag,propose(var_A,[_653467],var_AgI))),retractall(ext_agent(var_Ag,_653505,var_Ontology,_653509)).

call_propose(var_A,var_C,var_Ag):-clause(agent(var_AgI),var__),clause(ext_agent(var_Ag,_653303,var_Ontology,_653307),_653297),asserisci_ontologia(var_Ag,var_Ontology,var_A),once(call_meta_execute_propose(var_A,var_C,var_Ag)),a(message(var_Ag,accept_proposal(var_A,[],var_AgI))),retractall(ext_agent(var_Ag,_653373,var_Ontology,_653377)).

call_propose(var_A,var_C,var_Ag):-clause(agent(var_AgI),var__),clause(ext_agent(var_Ag,_653191,var_Ontology,_653195),_653185),not(call_meta_execute_propose(var_A,var_C,var_Ag)),a(message(var_Ag,reject_proposal(var_A,[],var_AgI))),retractall(ext_agent(var_Ag,_653247,var_Ontology,_653251)).

call_accept_proposal(var_A,var_Mp,var_Ag,var_T):-asse_cosa(past_event(accepted_proposal(var_A,var_Mp,var_Ag),var_T)),statistics(walltime,[var_Tp,var__]),retractall(past(accepted_proposal(var_A,var_Mp,var_Ag),var__,var_Ag)),assert(past(accepted_proposal(var_A,var_Mp,var_Ag),var_Tp,var_Ag)).

call_reject_proposal(var_A,var_Mp,var_Ag,var_T):-asse_cosa(past_event(rejected_proposal(var_A,var_Mp,var_Ag),var_T)),statistics(walltime,[var_Tp,var__]),retractall(past(rejected_proposal(var_A,var_Mp,var_Ag),var__,var_Ag)),assert(past(rejected_proposal(var_A,var_Mp,var_Ag),var_Tp,var_Ag)).

call_failure(var_A,var_M,var_Ag,var_T):-asse_cosa(past_event(failed_action(var_A,var_M,var_Ag),var_T)),statistics(walltime,[var_Tp,var__]),retractall(past(failed_action(var_A,var_M,var_Ag),var__,var_Ag)),assert(past(failed_action(var_A,var_M,var_Ag),var_Tp,var_Ag)).

call_cancel(var_A,var_Ag):-if(clause(high_action(var_A,var_Te,var_Ag),_652755),retractall(high_action(var_A,var_Te,var_Ag)),true),if(clause(normal_action(var_A,var_Te,var_Ag),_652789),retractall(normal_action(var_A,var_Te,var_Ag)),true).

external_refused_action_propose(var_A,var_Ag):-clause(not_executable_action_propose(var_A,var_Ag),var__).

evi(external_refused_action_propose(var_A,var_Ag)):-clause(agent(var_Ai),var__),a(message(var_Ag,failure(var_A,motivation(false_conditions),var_Ai))),retractall(not_executable_action_propose(var_A,var_Ag)).

refused_message(var_AgM,var_Con):-clause(eliminated_message(var_AgM,var__,var__,var_Con,var__),var__).

refused_message(var_To,var_M):-clause(eliminated_message(var_M,var_To,motivation(conditions_not_verified)),_652571).

evi(refused_message(var_AgM,var_Con)):-clause(agent(var_Ai),var__),a(message(var_AgM,inform(var_Con,motivation(refused_message),var_Ai))),retractall(eliminated_message(var_AgM,var__,var__,var_Con,var__)),retractall(eliminated_message(var_Con,var_AgM,motivation(conditions_not_verified))).

send_jasper_return_message(var_X,var_S,var_T,var_S0):-clause(agent(var_Ag),_652419),a(message(var_S,send_message(sent_rmi(var_X,var_T,var_S0),var_Ag))).

gest_learn(var_H):-clause(past(learn(var_H),var_T,var_U),_652367),learn_if(var_H,var_T,var_U).

evi(gest_learn(var_H)):-retractall(past(learn(var_H),_652243,_652245)),clause(agente(_652265,_652267,_652269,var_S),_652261),name(var_S,var_N),append(var_L,[46,112,108],var_N),name(var_F,var_L),manage_lg(var_H,var_F),a(learned(var_H)).

cllearn:-clause(agente(_652037,_652039,_652041,var_S),_652033),name(var_S,var_N),append(var_L,[46,112,108],var_N),append(var_L,[46,116,120,116],var_To),name(var_FI,var_To),open(var_FI,read,_652137,[]),repeat,read(_652137,var_T),arg(1,var_T,var_H),write(var_H),nl,var_T==end_of_file,!,close(_652137).

send_msg_learn(var_T,var_A,var_Ag):-a(message(var_Ag,confirm(learn(var_T),var_A))).

told(var_From,send_message(var_M)):-true.

told(var_Ag,execute_proc(var__)):-true.

told(var_Ag,query_ref(var__,var__)):-true.

told(var_Ag,agree(var__)):-true.

told(var_Ag,confirm(var__),200):-true.

told(var_Ag,disconfirm(var__)):-true.

told(var_Ag,request(var__,var__)):-true.

told(var_Ag,propose(var__,var__)):-true.

told(var_Ag,accept_proposal(var__,var__),20):-true.

told(var_Ag,reject_proposal(var__,var__),20):-true.

told(var__,failure(var__,var__),200):-true.

told(var__,cancel(var__)):-true.

told(var_Ag,inform(var__,var__),70):-true.

told(var_Ag,inform(var__),70):-true.

told(var_Ag,reply(var__)):-true.

told(var__,refuse(var__,var_Xp)):-functor(var_Xp,var_Fp,var__),var_Fp=agree.

tell(var_To,var_From,send_message(var_M)):-true.

tell(var_To,var__,confirm(var__)):-true.

tell(var_To,var__,disconfirm(var__)):-true.

tell(var_To,var__,propose(var__,var__)):-true.

tell(var_To,var__,request(var__,var__)):-true.

tell(var_To,var__,execute_proc(var__)):-true.

tell(var_To,var__,agree(var__)):-true.

tell(var_To,var__,reject_proposal(var__,var__)):-true.

tell(var_To,var__,accept_proposal(var__,var__)):-true.

tell(var_To,var__,failure(var__,var__)):-true.

tell(var_To,var__,query_ref(var__,var__)):-true.

tell(var_To,var__,eve(var__)):-true.

tell(var__,var__,refuse(var_X,var__)):-functor(var_X,var_F,var__),(var_F=send_message;var_F=query_ref).

tell(var_To,var__,inform(var__,var_M)):-true;var_M=motivation(refused_message).

tell(var_To,var__,inform(var__)):-true,var_To\=user.

tell(var_To,var__,propose_desire(var__,var__)):-true.

meta(var_P,var_V,var_AgM):-functor(var_P,var_F,var_N),var_N=0,clause(agent(var_Ag),var__),clause(ontology(var_Pre,[var_Rep,var_Host],var_Ag),var__),if((eq_property(var_F,var_V,var_Pre,[var_Rep,var_Host]);same_as(var_F,var_V,var_Pre,[var_Rep,var_Host]);eq_class(var_F,var_V,var_Pre,[var_Rep,var_Host])),true,if(clause(ontology(var_PreM,[var_RepM,var_HostM],var_AgM),var__),if((eq_property(var_F,var_V,var_PreM,[var_RepM,var_HostM]);same_as(var_F,var_V,var_PreM,[var_RepM,var_HostM]);eq_class(var_F,var_V,var_PreM,[var_RepM,var_HostM])),true,false),false)).

meta(var_P,var_V,var_AgM):-functor(var_P,var_F,var_N),(var_N=1;var_N=2),clause(agent(var_Ag),var__),clause(ontology(var_Pre,[var_Rep,var_Host],var_Ag),var__),if((eq_property(var_F,var_H,var_Pre,[var_Rep,var_Host]);same_as(var_F,var_H,var_Pre,[var_Rep,var_Host]);eq_class(var_F,var_H,var_Pre,[var_Rep,var_Host])),true,if(clause(ontology(var_PreM,[var_RepM,var_HostM],var_AgM),var__),if((eq_property(var_F,var_H,var_PreM,[var_RepM,var_HostM]);same_as(var_F,var_H,var_PreM,[var_RepM,var_HostM]);eq_class(var_F,var_H,var_PreM,[var_RepM,var_HostM])),true,false),false)),var_P=..var_L,substitute(var_F,var_L,var_H,var_Lf),var_V=..var_Lf.

meta(var_P,var_V,var__):-functor(var_P,var_F,var_N),var_N=2,symmetric(var_F),var_P=..var_L,delete(var_L,var_F,var_R),reverse(var_R,var_R1),append([var_F],var_R1,var_R2),var_V=..var_R2.

meta(var_P,var_V,var_AgM):-clause(agent(var_Ag),var__),functor(var_P,var_F,var_N),var_N=2,(symmetric(var_F,var_AgM);symmetric(var_F)),var_P=..var_L,delete(var_L,var_F,var_R),reverse(var_R,var_R1),clause(ontology(var_Pre,[var_Rep,var_Host],var_Ag),var__),if((eq_property(var_F,var_Y,var_Pre,[var_Rep,var_Host]);same_as(var_F,var_Y,var_Pre,[var_Rep,var_Host]);eq_class(var_F,var_Y,var_Pre,[var_Rep,var_Host])),true,if(clause(ontology(var_PreM,[var_RepM,var_HostM],var_AgM),var__),if((eq_property(var_F,var_Y,var_PreM,[var_RepM,var_HostM]);same_as(var_F,var_Y,var_PreM,[var_RepM,var_HostM]);eq_class(var_F,var_Y,var_PreM,[var_RepM,var_HostM])),true,false),false)),append([var_Y],var_R1,var_R2),var_V=..var_R2.

meta(var_P,var_V,var_AgM):-clause(agent(var_Ag),var__),clause(ontology(var_Pre,[var_Rep,var_Host],var_Ag),var__),functor(var_P,var_F,var_N),var_N>2,if((eq_property(var_F,var_H,var_Pre,[var_Rep,var_Host]);same_as(var_F,var_H,var_Pre,[var_Rep,var_Host]);eq_class(var_F,var_H,var_Pre,[var_Rep,var_Host])),true,if(clause(ontology(var_PreM,[var_RepM,var_HostM],var_AgM),var__),if((eq_property(var_F,var_H,var_PreM,[var_RepM,var_HostM]);same_as(var_F,var_H,var_PreM,[var_RepM,var_HostM]);eq_class(var_F,var_H,var_PreM,[var_RepM,var_HostM])),true,false),false)),var_P=..var_L,substitute(var_F,var_L,var_H,var_Lf),var_V=..var_Lf.

meta(var_P,var_V,var_AgM):-clause(agent(var_Ag),var__),clause(ontology(var_Pre,[var_Rep,var_Host],var_Ag),var__),functor(var_P,var_F,var_N),var_N=2,var_P=..var_L,if((eq_property(var_F,var_H,var_Pre,[var_Rep,var_Host]);same_as(var_F,var_H,var_Pre,[var_Rep,var_Host]);eq_class(var_F,var_H,var_Pre,[var_Rep,var_Host])),true,if(clause(ontology(var_PreM,[var_RepM,var_HostM],var_AgM),var__),if((eq_property(var_F,var_H,var_PreM,[var_RepM,var_HostM]);same_as(var_F,var_H,var_PreM,[var_RepM,var_HostM]);eq_class(var_F,var_H,var_PreM,[var_RepM,var_HostM])),true,false),false)),substitute(var_F,var_L,var_H,var_Lf),var_V=..var_Lf.
